;(function() {
    var application = angular.module("simple-cart", ["ui.router"])
        .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", "$http"
            function($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider, $http) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');

            $urlRouterProvider
                .otherwise('/');

            $stateProvider.decorator('views', function (state, parent) {
                var result = {},
                    views = parent(state);

                  angular.forEach(views, function (config, name) {
                      var autoName = (state.name + '.' + name).replace('.', '/');
                      config.templateUrl = config.templateUrl || '/application/partials/' + autoName + '.html';
                      result[name] = config;
                  });
                  return result;
            });

            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "application/partials/index.html",
                    controller: "indexCtrl",
                    resolve: {
                        produtos: function($http) {
                            return $http.get("/api/produtos").then(function(response) { return response; });
                        }
                    }
                })
                .state("carrinho", {
                    url: "/carrinho"
                })
                .state("carrinho.checkout", {
                    url: "/carrinho/checkout"
                });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        }]);
})();
