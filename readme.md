# Simple Cart - Startup Test

1. Criar banco de dados "startup_test"

2. Executar no terminal "composer install"

3. Executar no terminal "php artisan migrate --seed"

4. Executar no terminal "php artisan serve"

5. Acesse o endereço localhost:8000
