var gulp = require('gulp'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    cleanCSS = require('gulp-clean-css');

gulp.task("clean:build", function() {
    return gulp.src('public/assets')
                .pipe(clean());
});

gulp.task("verify:js", function() {
    return gulp.src([
        'resources/assets/scripts'
    ])
    .pipe(jshint())
    .pipe(jshint.reporter("default"));
});

gulp.task('compile:js', ['clean:build'], function() {
    //Concat sources
    return gulp.src(['resources/assets/scripts/**/*.js'])
                .pipe(uglify())
                .pipe(gulp.dest('public/assets/js'));
});

gulp.task('compile:css', ['clean:build'], function() {
    return gulp.src([
        'resources/assets/css/**/*.css'
    ])
    .pipe(cleanCSS())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('public/assets/css'));
});

gulp.task('copy', ['clean:build'], function() {
    return gulp.src([
        'resources/assets/images/*'
    ])
    .pipe(gulp.dest('public/assets/img'));
});

// gulp.task('watch', function() {
//     gulp.watch('resources/assets/**/*.js', ['verify:js', 'compile:js']);
//     gulp.watch('resources/assets/**/*.css', ['compile:css']);
// });

// gulp.task('default', ['clean:build', 'verify:js', 'compile:js', 'compile:css', 'copy', 'watch']);
gulp.task('default', ['clean:build', 'verify:js', 'compile:js', 'compile:css', 'copy']);
