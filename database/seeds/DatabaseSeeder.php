<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $caracteristicas = factory(App\Models\Caracteristica::class, 5)->make();

        $categorias = factory(App\Models\Categoria::class, 5)->make();

        factory(App\Models\Produto::class, 20)
            ->create()
            ->each(function($produto) use ($caracteristicas, $categorias) {
                $qtd = rand(0, 4);
                $produto->caracteristicas()->saveMany($caracteristicas->take($qtd));
                $produto->categorias()->saveMany($categorias->take($qtd));
            });
    }
}
