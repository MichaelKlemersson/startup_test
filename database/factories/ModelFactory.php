<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Produto::class, function (Faker\Generator $faker) {
    $name = $faker->sentence($nbWords = 4, $variableNbWords = true);
    return [
        'nome' => $name,
        'descricao' => $faker->text($maxNbChars = 200),
        'slug' => str_slug($name, '-'),
        'preco' => ($faker->randomFloat(2, 1, 1000))
    ];
});

$factory->define(App\Models\Categoria::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'descricao' => $faker->text($maxNbChars = 140),
    ];
});

$factory->define(App\Models\Caracteristica::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'descricao' => $faker->text($maxNbChars = 140),
    ];
});
