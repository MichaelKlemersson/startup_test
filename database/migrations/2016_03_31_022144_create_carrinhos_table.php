<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrinhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrinhos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned()->index();
            $table->boolean('status')->index();
            $table->timestamps();
        });

        Schema::create('carrinhos_itens', function (Blueprint $table) {
            $table->integer('produto_id')->unsigned()->index();
            $table->integer('carrinho_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrinhos');
        Schema::drop('carrinhos_itens');
    }
}
