<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string("cliente_id")->index();
            $table->timestamps();
        });

        Schema::create('pedidos_produtos', function (Blueprint $table) {
            $table->integer("pedido_id")->index();
            $table->integer("produto_id")->index();
            $table->integer("quantidade")->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos');
        Schema::drop('pedidos_produtos');
    }
}
