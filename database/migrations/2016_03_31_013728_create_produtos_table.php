<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('descricao');
            $table->string('slug')->unique();
            $table->double('preco', 10, 2)->index();
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('produtos_caracteristicas', function (Blueprint $table) {
            $table->integer('produto_id')->index();
            $table->integer('caracteristica_id')->index();
        });

        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->integer('produto_id')->index();
            $table->integer('categoria_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
        Schema::drop('produtos_caracteristicas');
        Schema::drop('produtos_categorias');
    }
}
