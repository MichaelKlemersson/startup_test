<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model as Eloquent;

abstract class AbstractRepository extends Eloquent {

}
