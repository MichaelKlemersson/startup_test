<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model
{
    protected $table = "carrinhos";

    protected $guarded = ['id'];

    protected $fillable = [
        'produto_id',
        'status',
        'cliente_id'
    ];

    const
        ABERTO = 1,
        FECHADO = 0;

    public function itens()
    {
        return $this->hasMany(__NAMESPACE__ . '\Produto', 'produto_id');
    }

    public function isAberto()
    {
        return $this->attributes['status'] === self::ABERTO;
    }
}
