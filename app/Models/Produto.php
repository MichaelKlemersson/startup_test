<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = "produtos";

    protected $guarded = ['id'];

    protected $fillable = [
        'nome',
        'preco'
    ];

    public function categorias()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\Categoria', 'produtos_categorias', 'produto_id');
    }

    public function caracteristicas()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\Caracteristica', 'produtos_caracteristicas', 'produto_id');
    }
}
