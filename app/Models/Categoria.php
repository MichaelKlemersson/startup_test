<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categorias";

    protected $guarded = ['id'];

    protected $fillable = [
        'nome', 'descricao', 'categoria_pai'
    ];

    public function categoriaPai()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Categoria', 'categoria_pai');
    }

    public function subCategorias()
    {
        return $this->hasMany(__NAMESPACE__ . '\Categoria', 'categoria_pai');
    }

    public function produtos()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\Produto', 'produtos_categorias', 'categoria_id');
    }
}
