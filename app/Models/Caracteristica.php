<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{
    protected $table = "caracteristicas";

    protected $guarded = ["id"];

    protected $fillable = [
        "nome", "descricao"
    ];
}
