<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = "pedidos";

    protected $guarded = ["id"];

    protected $fillable = [
        "client_id"
    ];

    public function produtos()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\Produto', 'pedidos_produtos', 'pedido_id');
    }
}
