<?php

namespace App\Api\Http\Controllers;

use Illuminate\Http\Request;

use App\Core\Http\Requests;
use App\Core\Http\Controllers\Controller;
use App\Models\Produto;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryProduto = Produto::query();
        if($request->input("cat", false)) {
            $categorias = explode(",", $request->input("cat"));
            $queryProduto = $queryProduto->whereHas("categorias", function($query) use ($categorias, $request) {
                $query->whereIn("categorias.id", $categorias);
            });
        }
        if($request->input("nome", false) && ("" !== $request->input("nome", ""))) {
            $nome = $request->input('nome');
            $queryProduto = $queryProduto->where("nome", "like", "%{$nome}%");
        }
        if($request->input("id", false)) {
            $ids = explode(",", $request->input("id"));
            $queryProduto = $queryProduto->whereIn("id", $ids);
        }
        return $queryProduto->paginate(20, ["id", "nome", "preco", "slug"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
