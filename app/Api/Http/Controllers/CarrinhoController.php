<?php

namespace App\Api\Http\Controllers;

use Illuminate\Http\Request;

use App\Core\Http\Requests;
use App\Core\Http\Controllers\Controller;
use App\Models\Pedido;
use App\Models\Produto;

class CarrinhoController extends Controller
{
    public function checkout(Request $request)
    {
        try {
            $pedido = Pedido::create();
            $pedido->cliente_id = $request->input("cliente");
            foreach($request->input("produtos") as $dataProduto) {
                $pedido->produtos()->attach(Produto::find($dataProduto["id"]), ["quantidade" => (isset($dataProduto["quantidade"]) ? ($dataProduto["quantidade"] > 0 ? $dataProduto["quantidade"] : ($dataProduto["quantidade"] * -1)) : 1)]);
            }
            $pedido->save();
            return response()->json([
                "success" => true, "message" => "Pedido salvo com sucesso",
                "info" => [
                    "pedido" => $pedido
                ]
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                "success" => false, "message" => "Pedido não pode ser salvo",
                "info" => [
                    "exception" => $e->getMessage()
                ]
            ]);
        }
    }
}
