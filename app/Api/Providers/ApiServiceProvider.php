<?php
namespace App\Api\Providers;


use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public function boot(Router $router)
    {
        $this->registerRoutes($router);
    }

    public function register()
    {

    }

    public function registerRoutes(Router $router)
    {
        $router->group([
            'namespace' => 'App\Api\Http\Controllers',
            'prefix' => 'api'
        ], function($router) {
            require app_path('Api/Http/routes.php');
        });
    }
}
