<?php
namespace App\Site\Providers;


use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SiteServiceProvider extends ServiceProvider
{
    public function boot(Router $router)
    {
        $this->registerRoutes($router);
    }

    public function register()
    {

    }

    protected function registerRoutes(Router $router)
    {
        $router->group([
            'namespace' => 'App\Site\Http\Controllers',
        ], function($router) {
            require app_path('Site/Http/routes.php');
        });
    }
}
