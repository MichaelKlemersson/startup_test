<?php

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::any('{path?}', "IndexController@index");

Route::group(["prefix" => "carrinho"], function() {
    Route::group(["prefix" => "produto"], function() {
        Route::post("add", "CarrinhoController@addProduto");
        Route::delete("delete/{produto_id}", "CarrinhoController@deleteProduto");
    });

    Route::get("ultimo-carrinho", "CarrinhoController@getUltimoCarrinhoAberto");
    Route::post("clear", "CarrinhoController@clear");
});
