<?php

namespace App\Site\Http\Controllers;

use Illuminate\Http\Request;
use App\Core\Http\Requests;
use App\Core\Http\Controllers\Controller;
use Illuminate\Cookie\CookieJar;

class IndexController extends Controller
{
    private $cookie;

    public function __construct(CookieJar $cookieManager)
    {
        $this->cookie = \Cookie::get("carrinho");
        if($this->cookie == null) {
            $this->cookie = $cookieManager->make("carrinho", serialize([
                "cliente" => base64_encode(microtime() + floor(rand()*10000)),
                "produtos" => []
            ]), (60 * 24 * 30));
        }
        else {
            #echo "Cookie exists"; exit;
            $this->cookie = cookie("carrinho", $this->cookie);
        }
    }

    public function index()
    {
        #dd($this->cookie);
        return response()->view("layout")->withCookie($this->cookie);
    }
}
