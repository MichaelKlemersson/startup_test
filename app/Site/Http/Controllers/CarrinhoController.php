<?php

namespace App\Site\Http\Controllers;

use Illuminate\Http\Request;

use App\Core\Http\Requests;
use App\Core\Http\Controllers\Controller;
use Illuminate\Cookie\CookieJar;

class CarrinhoController extends Controller
{
    private $cookie;

    public function __construct()
    {
        $this->cookie = \Cookie::get("carrinho");
        if($this->cookie != null) {
            $this->cookie = unserialize($this->cookie);
        }
    }

    public function addProduto(Request $request, CookieJar $cookieManager)
    {
        if(!isset($this->cookie["produtos"]))
            $this->cookie["produtos"] = array();

        $dataProduto = $request->input("produto");
        if(isset($this->cookie["produtos"][$dataProduto["id"]])) {
            $this->cookie["produtos"][$dataProduto["id"]] += $request->input("qtd", 1);
        }
        else {
            $this->cookie["produtos"][$dataProduto["id"]] = 1;
        }
        #$_COOKIE["carrinho"] = serialize($this->cookie);
        $cookieManager->forget("carrinho");
        $cookie = $cookieManager->make("carrinho", serialize($this->cookie), (60 * 24 * 30));
        return response()->json([
            "success" => true,
            "message" => "Produto adicionado com sucesso",
            "info" => [
                "carrinho" => $this->cookie["produtos"]
            ]
        ])->withCookie($cookie);
    }

    public function deleteProduto($produto_id, CookieJar $cookieManager)
    {
        if(isset($this->cookie->produtos[$produto_id])) {
            unset($this->cookie->produtos[$produto_id]);
        }
        $cookieManager->forget("carrinho");
        $cookie = $cookieManager->make("carrinho", serialize($this->cookie), (60 * 24 * 30));
        #\Cookie::make("carrinho", serialize($this->cookie), (60 * 24 * 30));
        return response()->json([
            "success" => true,
            "message" => "Produto removido com sucesso",
            "info" => [
                "carrinho" => $this->cookie["produtos"]
            ]
        ])->withCookie($cookie);
    }

    public function clear(Request $request, CookieJar $cookieManager)
    {
        $cookieManager->forget("carrinho");
        $this->cookie = [
            "cliente" => base64_encode(microtime() + floor(rand()*10000)),
            "produtos" => []
        ];
        $cookie = $cookieManager->make("carrinho", serialize($this->cookie), (60 * 24 * 30));
        return response()->json([
            "success" => true,
            "info" => [
                "carrinho" => $this->cookie["produtos"]
            ]
        ])->withCookie($cookie);
    }

    public function getUltimoCarrinhoAberto()
    {
        return response()->json(unserialize(\Cookie::get("carrinho")));
    }
}
