angular.module("simple-cart").service("categoriaService", function($http, baseApiUrl) {
    this.getCategorias = function(params) {
        return $http.get(baseApiUrl + "/categorias", params);
    }
});
