angular.module("simple-cart").service("produtoService", function($http, baseApiUrl) {
    this.getProdutos = function(params) {
        return $http.get(baseApiUrl + "/produtos", { params: params });
    }
});
