<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Carrinho - Test</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/css/fakeLoader.css" media="screen" title="no title" charset="utf-8">
        <link rel="stylesheet" href="/assets/css/styles.css" media="screen" title="no title" charset="utf-8">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="/assets/js/fakeLoader.min.js" charset="utf-8"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $(".fakeloader").fakeLoader({
                timeToHide:2200,
                bgColor:"#34495e",
                spinner:"spinner4"
            });
        });
        </script>
    </head>
    <body ng-app="simple-cart">
        <div class="fakeloader"></div>
        <header ui-view="header">

        </header>

        <section ui-view="content" id="wrap-content">

        </section>

        <footer ui-view="footer">

        </footer>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.min.js" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/i18n/angular-locale_pt-br.js" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular-messages.min.js" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.18/angular-ui-router.min.js" charset="utf-8"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular-animate.min.js" charset="utf-8"></script> -->

        <script src="/application/app.js"></script>
        <script src="/application/services/produtoService.js"></script>
        <script src="/application/services/categoriaService.js"></script>
        <script src="/application/controllers/indexCtrl.js"></script>
        <script src="/application/controllers/sessionCtrl.js"></script>
        <script src="/application/controllers/carrinhoCtrl.js"></script>
    </body>
</html>
