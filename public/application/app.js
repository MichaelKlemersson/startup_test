;(function() {
    var application = angular.module("simple-cart", ["ui.router"])
        .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider",
            function($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');

            $urlRouterProvider
                // .when('/carrinho', "carrinho")
                .otherwise('/');

            $stateProvider.decorator('views', function (state, parent) {
                var result = {},
                    views = parent(state);

                  angular.forEach(views, function (config, name) {
                      var autoName = (state.name + '.' + name).replace('.', '/');
                      config.templateUrl = config.templateUrl || '/application/partials/' + autoName + '.html';
                      result[name] = config;
                  });
                  return result;
            });

            $stateProvider
                .state("root", {
                    url: "",
                    abstract: true,
                    views: {
                        "layout": {
                            templateUrl: "application/partials/static/layout.html"
                        },
                        "header@": {
                            templateUrl: "application/partials/static/header.html",
                            controller: "sessionCtrl"
                         }
                    }

                })
                .state("home", {
                    url: "/?cat",
                    parent: "root",
                    views: {
                        "content@": {
                            templateUrl: "application/partials/index.html",
                            controller: "indexCtrl"
                        }
                    },
                    resolve: {
                        produtosPromise: function(produtoService, $stateParams) {
                            return produtoService.getProdutos($stateParams);
                        },
                        categoriasPromise: function(categoriaService) {
                            return categoriaService.getCategorias();
                        }
                    }
                })
                .state("carrinho", {
                    url: "/carrinho",
                    parent: "root",
                    views: {
                        "content@": {
                            templateUrl: "application/partials/carrinho.html",
                            controller: "carrinhoCtrl"
                        }
                    }
                });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        }]);

        application.constant("baseApiUrl", "/api");

        application.run(function($rootScope, $state) {
            $rootScope.$state = $state;
        });
})();
