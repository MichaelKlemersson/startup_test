angular.module("simple-cart").controller("carrinhoCtrl", function($scope, $http, produtoService, baseApiUrl, $state, $rootScope) {
    $scope.produtosNoCarrinho = [];
    $scope.produtosSelecionados = [];
    $scope.totalCarrinho = 0;
    $scope.cliente = null;

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.produtosNoCarrinho.length; i++){
            var product = $scope.produtosNoCarrinho[i];
            total += (product.preco * product.quantidade);
        }
        return total;
    };

    $scope.selecionaProduto = function($event, produto) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        if (action == 'add' & $scope.produtosSelecionados.indexOf(produto.id) == -1) $scope.produtosSelecionados.push(produto.id);
        if (action == 'remove' && $scope.produtosSelecionados.indexOf(produto.id) != -1) $scope.produtosSelecionados.splice($scope.produtosSelecionados.indexOf(produto.id), 1);
    };

    $scope.efetuarCheckout = function() {
        $http.post(baseApiUrl + "/carrinho/checkout", { produtos: $scope.produtosNoCarrinho, cliente: $scope.cliente })
            .then(function(response) {
                if(response.data.success === true) {
                    $http.post("/carrinho/clear", { cliente: $scope.cliente })
                        .then(function(res) {
                            if(res.data.success === true) {
                                $rootScope.$broadcast("clearCookieData", true);
                                alert(response.data.message);
                                $state.go("home");
                            }
                        }, function(err) {
                            console.error(err);
                        });
                }
            }, function(error) {
                console.error(error);
            });
    };

    $http.get("/carrinho/ultimo-carrinho").then(function(response) {
        if(typeof response.data === "object") {
            $scope.cliente = response.data.cliente;
            console.log($scope.cliente);
            console.log(response.data.cliente);
            if(Object.keys(response.data.produtos).length) {
                produtoService.getProdutos({ id: Object.keys(response.data.produtos).join(',') }).then(function(resProdutos) {
                    $.map(resProdutos.data.data, function(item) {
                        item.quantidade = response.data.produtos[item.id];
                    });
                    $scope.produtosNoCarrinho = resProdutos.data.data;
                }, function(err) {
                    console.error(err);
                });
            }
        }
    }, function(err) {
        console.error(err);
    });

    $scope.$on("redirectToCarrinho", function(event, data) {
        console.log("listen");
        $state.go("carrinho");
    });
});
