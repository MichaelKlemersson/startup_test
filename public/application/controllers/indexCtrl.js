angular.module("simple-cart").controller("indexCtrl", function($scope, produtosPromise, categoriasPromise, $rootScope, $http, baseApiUrl, $q) {
    $scope.produtos = (typeof categoriasPromise === "object" ? produtosPromise.data.data : []);
    $scope.categorias = (typeof categoriasPromise === "object" ? categoriasPromise.data : []);
    var isMakingRequest = false;

    $scope.addProduto = function(produto) {
        var refProduto = angular.copy(produto);
        $http.post("/carrinho/produto/add", { produto: refProduto }).then(function(response) {
            console.log(response);
            if(typeof response.data === "object") {
                if(response.data.success === true)
                    $rootScope.$broadcast("addProdutoCarrinho", angular.copy(produto));
            }
        }, function(error) {
            console.error(err);
        });
        delete produto;
    };

    $scope.filtraBusca = function($event, keyCode) {
        var requestDispatcher = $q.defer();
        if(!isMakingRequest && keyCode === 13) {
            isMakingRequest = true;
            $http.get(baseApiUrl + "/produtos", { params: { nome: $.trim($event.target.value) } })
                .then(function(response) {
                    isMakingRequest = false;
                    if(typeof response.data.data != "undefined") {
                        $scope.produtos = response.data.data;
                    }
                }, function(error) { isMakingRequest = false; console.error(error); });
        } else {
            requestDispatcher.resolve();
        }
    };

    $http.get("/carrinho/ultimo-carrinho").then(function(response) {
        console.log(response);
        if(typeof response.data === "object") {
            $rootScope.$broadcast("loadCookieData", response.data);
        }
    }, function(err) {
        console.error(err);
    });
});
