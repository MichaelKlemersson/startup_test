angular.module("simple-cart").controller("sessionCtrl", function($scope, $rootScope) {
    $scope.produtosSelecionados = [];
    $scope.totalCarrinho = $scope.produtosSelecionados.length;

    $scope.goToCarrinho = function() {
        $rootScope.$broadcast("redirectToCarrinho", true);
    };

    $scope.$on("loadCookieData", function(event, data) {
        if($scope.produtosSelecionados.length == 0 && typeof data.produtos != "undefined") {
            $scope.produtosSelecionados = data.produtos;
            $.each(data.produtos, function(index) {
                $scope.totalCarrinho += data.produtos[index];
            });
        }
    });

    $scope.$on("clearCookieData", function(event, data) {
        $scope.produtosSelecionados = [];
        $scope.totalCarrinho = 0;
    });

    $scope.$on("addProdutoCarrinho", function(event, data) {
        $scope.produtosSelecionados.push(data.id);
        $scope.totalCarrinho = $scope.produtosSelecionados.length;
    });
});
